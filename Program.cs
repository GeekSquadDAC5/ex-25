﻿using System;

namespace ex_25
{

    // a) Create a class (give it a name) that takes 2 fields and a constructor which sets the values to the fields. 
    // Make sure of the following : Fields are private Constructor is public
    // Create a method (public) that can print out the values of the Fields

    class ClassForEX25
    {
        private int Field1;
        private int Field2;


        public ClassForEX25(int num1, int num2)
        {
            Field1 = num1;
            Field2 = num2;

            this.PrintNumbers();
        }

        private void PrintNumbers()
        {
            Console.WriteLine($"Number1 : {Field1}, Number2 : {Field2}");
        }
    }

    // b) Create a class that with the name Checkout With the following items:
    // - Quantity : int
    // - Price : double

    // Constructor that sets the value for each of the Fields
    // Method that returns the value of a quantity times the price
    // In your main method add the total of 3 Equations.

    class Checkout
    {
        private int Quntity;
        private double Price;


        public Checkout(int quntity, double price)
        {
            this.Quntity = quntity;
            this.Price = price;
        }

        public double PrintTotal()
        {
            return  Convert.ToDouble(this.Quntity) * this.Price;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            // a) 
            var a = new ClassForEX25(4, 5);

            // b)
            var a1 = new Checkout(3, 4.50);
            var a2 = new Checkout(4, 5.50);
            var a3 = new Checkout(5, 6.50);
            var a4 = new Checkout(6, 7.50);
            var a5 = new Checkout(7, 8.50);

            Console.WriteLine(a1.PrintTotal()+a2.PrintTotal()+a3.PrintTotal()+a4.PrintTotal()+a5.PrintTotal());
        }
    }
}
